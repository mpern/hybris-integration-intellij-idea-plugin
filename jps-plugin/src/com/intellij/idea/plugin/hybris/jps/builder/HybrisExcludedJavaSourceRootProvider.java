/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.builder;

import com.intellij.idea.plugin.hybris.common.HybrisConstants;
import com.intellij.idea.plugin.hybris.jps.JpsUtil;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.VfsUtilCore;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.jps.builders.java.ExcludedJavaSourceRootProvider;
import org.jetbrains.jps.model.module.JpsModule;
import org.jetbrains.jps.model.module.JpsModuleSourceRoot;

import java.io.File;

/**
 * @author Eugene.Kudelevsky
 */
public class HybrisExcludedJavaSourceRootProvider extends ExcludedJavaSourceRootProvider {

    @Override
    public boolean isExcludedFromCompilation(@NotNull final JpsModule module, @NotNull final JpsModuleSourceRoot root) {
        if (JpsUtil.isHybrisModule(module)) {
            for (String contentRootUrl : module.getContentRootsList().getUrls()) {
                final File contentRoot = new File(VfsUtilCore.urlToPath(contentRootUrl));
                final File addOnDirectory = new File(contentRoot, HybrisConstants.ACCELERATOR_ADDON_DIRECTORY);

                if (FileUtil.isAncestor(addOnDirectory, root.getFile(), false)) {
                    return true;
                }
            }
        }
        return false;
    }
}
