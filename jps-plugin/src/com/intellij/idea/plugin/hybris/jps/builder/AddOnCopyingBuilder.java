/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.builder;

import com.intellij.idea.plugin.hybris.common.HybrisConstants;
import com.intellij.idea.plugin.hybris.jps.JpsUtil;
import com.intellij.idea.plugin.hybris.jps.model.AddOnResourcesRootDescriptor;
import com.intellij.idea.plugin.hybris.jps.model.AddOnResourcesTarget;
import com.intellij.openapi.util.Pair;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.util.containers.MultiMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.jps.builders.BuildOutputConsumer;
import org.jetbrains.jps.builders.DirtyFilesHolder;
import org.jetbrains.jps.incremental.CompileContext;
import org.jetbrains.jps.incremental.FSOperations;
import org.jetbrains.jps.incremental.ProjectBuildException;
import org.jetbrains.jps.incremental.StopBuildException;
import org.jetbrains.jps.incremental.TargetBuilder;
import org.jetbrains.jps.incremental.fs.CompilationRound;
import org.jetbrains.jps.incremental.messages.BuildMessage;
import org.jetbrains.jps.incremental.messages.CompilerMessage;
import org.jetbrains.jps.model.module.JpsModule;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Eugene.Kudelevsky
 */
public class AddOnCopyingBuilder extends TargetBuilder<AddOnResourcesRootDescriptor, AddOnResourcesTarget> {

    private static final String BUILDER_NAME = "Hybris Add-on Resources Compiler";

    public AddOnCopyingBuilder() {
        super(Collections.singletonList(AddOnResourcesTarget.MyTargetType.INSTANCE));
    }

    @Override
    public void build(
        @NotNull final AddOnResourcesTarget target,
        @NotNull final DirtyFilesHolder<AddOnResourcesRootDescriptor, AddOnResourcesTarget> holder,
        @NotNull final BuildOutputConsumer outputConsumer,
        @NotNull final CompileContext context
    ) throws ProjectBuildException, IOException {

        if (!holder.hasDirtyFiles() && !holder.hasRemovedFiles()) {
            return;
        }
        if (!doBuild(target, outputConsumer, context)) {
            //noinspection NewExceptionWithoutArguments
            throw new StopBuildException();
        }
    }

    private static boolean doBuild(
        final @NotNull AddOnResourcesTarget target,
        final @NotNull BuildOutputConsumer outputConsumer,
        final @NotNull CompileContext context
    ) throws IOException {
        final MultiMap<File, Pair<String, File>> dir2Files = buildFileMap(context, target);

        for (Map.Entry<File, Collection<Pair<String, File>>> entry : dir2Files.entrySet()) {
            final File outDir = entry.getKey();

            if (outDir.exists() && !FileUtil.delete(outDir)) {
                context.processMessage(new CompilerMessage(
                    BUILDER_NAME,
                    BuildMessage.Kind.ERROR,
                    "Cannot delete directory: " + outDir.getPath()
                ));
                return false;
            }
            if (!outDir.mkdirs()) {
                context.processMessage(new CompilerMessage(
                    BUILDER_NAME,
                    BuildMessage.Kind.ERROR,
                    "Cannot create directory: " + outDir.getPath()
                ));
                return false;
            }
            final Collection<Pair<String, File>> copyingInfos = entry.getValue();
            final File generatedSourcesStorage = JpsUtil.getGeneratedSourcesStorage(
                target.getModule(), context.getProjectDescriptor().dataManager.getDataPaths());

            for (Pair<String, File> copyingInfo : copyingInfos) {
                final String relativeOutPath = copyingInfo.getFirst();
                final File srcFile = copyingInfo.getSecond();
                final File outFile = new File(outDir, relativeOutPath).getCanonicalFile();
                try {
                    FileUtil.copyContent(srcFile, outFile);
                } catch (IOException e) {
                    context.processMessage(new CompilerMessage(
                        BUILDER_NAME,
                        BuildMessage.Kind.ERROR,
                        "Cannot copy file from \"" + srcFile.getPath() + "\" to \"" + outFile.getPath() + '"'
                    ));
                    return false;
                }
                if (FileUtil.isAncestor(generatedSourcesStorage, outFile, true)) {
                    // copied file is outside of monitored content roots, so we have to mark it dirty manually
                    FSOperations.markDirty(context, CompilationRound.CURRENT, outFile);
                }
            }
            outputConsumer.registerOutputDirectory(outDir, copyingInfos
                .stream()
                .map(pair -> pair.getSecond())
                .map(File::getAbsolutePath)
                .collect(Collectors.toList()));
        }
        return true;
    }

    @NotNull
    private static MultiMap<File, Pair<String, File>> buildFileMap(
        final @NotNull CompileContext context,
        final @NotNull AddOnResourcesTarget target
    ) {
        final JpsModule module = target.getModule();
        final File moduleRoot = JpsUtil.findHybrisModuleRoot(module);

        if (moduleRoot == null) {
            return MultiMap.empty();
        }
        final File resDestDir = new File(moduleRoot, HybrisConstants.WEB_ROOT_DIRECTORY_RELATIVE_PATH);
        final File generatedSourcesStorage = JpsUtil.getGeneratedSourcesStorage(
            module,
            context.getProjectDescriptor().dataManager.getDataPaths()
        );
        final File sourceDestDir = new File(generatedSourcesStorage, JpsUtil.ADD_ONS_DIR_NAME);
        final MultiMap<File, Pair<String, File>> dir2Files = MultiMap.create();

        context.getProjectDescriptor().getBuildRootIndex().getTargetRoots(target, context).forEach(descriptor -> {
            final File rootFile = descriptor.getRootFile();
            final String addOnName = descriptor.getAddOnName();

            if (descriptor.isResourceRoot()) {
                fillMapWithResources(rootFile, addOnName, resDestDir, dir2Files);
            } else {
                fillMapForCopying(rootFile, new File(sourceDestDir, addOnName), dir2Files);
            }
        });
        return dir2Files;
    }

    private static void fillMapWithResources(
        @NotNull final File rootFile,
        @NotNull final String addOnName,
        @NotNull final File outputDir,
        @NotNull final MultiMap<File, Pair<String, File>> map
    ) {
        final File webRootFile = new File(rootFile, HybrisConstants.WEB_ROOT_DIRECTORY);

        for (File subFolder : getSubFolders(webRootFile)) {
            final File outputDir1 = new File(outputDir, subFolder.getName());

            if (subFolder.getName().equals(HybrisConstants.WEB_INF_DIRECTORY)) {
                for (File subFolder1 : getSubFolders(subFolder)) {
                    fillMapForResourceCopying(subFolder1, new File(outputDir1, subFolder1.getName()), addOnName, map);
                }
            } else {
                fillMapForResourceCopying(subFolder, outputDir1, addOnName, map);
            }
        }
    }

    private static List<File> getSubFolders(@NotNull final File dir) {
        final File[] children = dir.listFiles();

        return children == null
            ? Collections.emptyList()
            : Arrays.stream(children).filter(File::isDirectory).collect(Collectors.toList());
    }

    private static void fillMapForResourceCopying(
        @NotNull final File sourceDir,
        @NotNull final File outputDir,
        @NotNull final String addOnName,
        @NotNull final MultiMap<File, Pair<String, File>> map
    ) {
        final File addOnOutputDir = new File(outputDir, "addons/" + addOnName);
        fillMapForCopying(sourceDir, addOnOutputDir, map);
    }

    private static void fillMapForCopying(
        final @NotNull File sourceDir,
        final @NotNull File outputDir,
        final @NotNull MultiMap<File, Pair<String, File>> map
    ) {
        FileUtil.processFilesRecursively(sourceDir, file -> {
            if (file.isFile()) {
                final File parentDir = file.getParentFile();

                if (parentDir != null) {
                    String relativePath = FileUtil.getRelativePath(sourceDir, parentDir);

                    if (relativePath != null) {
                        relativePath = FileUtil.toSystemIndependentName(relativePath);
                        map.putValue(outputDir, Pair.create(relativePath + '/' + file.getName(), file));
                    }
                }
            }
            return true;
        });
    }

    @NotNull
    @Override
    public String getPresentableName() {
        return BUILDER_NAME;
    }
}
