/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.model;

import com.intellij.util.xmlb.XmlSerializer;
import org.jdom.Element;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.jps.model.JpsProject;
import org.jetbrains.jps.model.serialization.JpsProjectExtensionSerializer;

/**
 * @author Eugene.Kudelevsky
 */
public class JpsHybrisProjectExtensionSerializer extends JpsProjectExtensionSerializer {

    public JpsHybrisProjectExtensionSerializer() {
        super("hybrisProjectSettings.xml", "HybrisProjectSettings");
    }

    @Override
    public void loadExtension(@NotNull final JpsProject jpsProject, @NotNull final Element componentTag) {
        final JpsHybrisProjectExtension extension = JpsHybrisProjectExtension.getOrCreateExtension(jpsProject);
        extension.setState(XmlSerializer.deserialize(componentTag, JpsHybrisProjectSettingsState.class));
    }

    @Override
    public void saveExtension(@NotNull final JpsProject jpsProject, @NotNull final Element componentTag) {
    }
}
