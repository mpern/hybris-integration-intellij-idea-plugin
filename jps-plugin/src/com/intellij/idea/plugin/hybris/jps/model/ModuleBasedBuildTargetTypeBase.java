/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.model;

import com.intellij.idea.plugin.hybris.jps.JpsUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.jps.builders.BuildTargetLoader;
import org.jetbrains.jps.builders.ModuleBasedBuildTargetType;
import org.jetbrains.jps.builders.ModuleBasedTarget;
import org.jetbrains.jps.model.JpsModel;
import org.jetbrains.jps.model.JpsNamedElement;
import org.jetbrains.jps.model.module.JpsModule;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Eugene.Kudelevsky
 */
public abstract class ModuleBasedBuildTargetTypeBase<T extends ModuleBasedTarget<?>>
    extends ModuleBasedBuildTargetType<T> {

    public ModuleBasedBuildTargetTypeBase(final String typeId) {
        super(typeId);
    }

    @NotNull
    protected abstract T createTarget(@NotNull JpsModule module);

    @NotNull
    @Override
    public List<T> computeAllTargets(@NotNull final JpsModel model) {
        return model
            .getProject().getModules().stream()
            .filter(this::isAcceptableModule)
            .map(this::createTarget)
            .collect(Collectors.toList());
    }

    protected boolean isAcceptableModule(@NotNull final JpsModule module) {
        return JpsUtil.isHybrisModule(module);
    }

    @NotNull
    @Override
    public BuildTargetLoader<T> createLoader(@NotNull final JpsModel model) {
        final Map<String, JpsModule> moduleMap = model.getProject().getModules().stream().collect(
            Collectors.toMap(
                JpsNamedElement::getName,
                Function.identity()
            ));
        //noinspection ReturnOfInnerClass
        return new MyBuildTargetLoader(moduleMap);
    }

    private class MyBuildTargetLoader extends BuildTargetLoader<T> {

        @NotNull
        private final Map<String, JpsModule> moduleMap;

        public MyBuildTargetLoader(@NotNull final Map<String, JpsModule> moduleMap) {
            this.moduleMap = moduleMap;
        }

        @Nullable
        @Override
        public T createTarget(@NotNull final String targetId) {
            final JpsModule module = moduleMap.get(targetId);
            return module != null ? ModuleBasedBuildTargetTypeBase.this.createTarget(module) : null;
        }
    }
}
