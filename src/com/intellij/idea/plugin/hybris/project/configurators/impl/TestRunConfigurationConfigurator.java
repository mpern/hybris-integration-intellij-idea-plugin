/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.project.configurators.impl;

import com.intellij.execution.RunnerAndConfigurationSettings;
import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.impl.RunManagerImpl;
import com.intellij.idea.plugin.hybris.project.configurators.RunConfigurationConfigurator;
import com.intellij.idea.plugin.hybris.project.descriptors.HybrisProjectDescriptor;
import com.intellij.idea.plugin.hybris.runConfigurations.ClassAnnotationTestFilter;
import com.intellij.idea.plugin.hybris.runConfigurations.HybrisTestConfiguration;
import com.intellij.idea.plugin.hybris.runConfigurations.HybrisTestConfigurationType;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

import static com.intellij.idea.plugin.hybris.runConfigurations.ClassAnnotationTestFilter.INTEGRATION_TESTS;

/**
 * Created by Martin Zdarsky-Jones (martin.zdarsky@hybris.com) on 17/10/2016.
 */
public class TestRunConfigurationConfigurator implements RunConfigurationConfigurator {

    @Override
    public void configure(
        @NotNull final HybrisProjectDescriptor hybrisProjectDescriptor,
        @NotNull final Project project
    ) {

        final RunManagerImpl runManager = RunManagerImpl.getInstanceImpl(project);

        Arrays.stream(ClassAnnotationTestFilter.values()).forEach(filter -> addRunConfiguration(runManager, filter));
    }

    private void addRunConfiguration(
        @NotNull final RunManagerImpl runManager,
        @NotNull final ClassAnnotationTestFilter annotationFilter
    ) {
        final ConfigurationFactory factory = HybrisTestConfigurationType.getInstance().getConfigurationFactories()[0];
        final RunnerAndConfigurationSettings runnerAndSettings = runManager.createConfiguration(
            annotationFilter.getName(),
            factory
        );
        //noinspection CastToConcreteClass
        final HybrisTestConfiguration configuration = (HybrisTestConfiguration) runnerAndSettings.getConfiguration();
        configuration.setClassAnnotationFilter(annotationFilter);
        runnerAndSettings.setSingleton(true);
        if (annotationFilter == INTEGRATION_TESTS) {
            runManager.setSelectedConfiguration(runnerAndSettings);
        }
        runManager.addConfiguration(runnerAndSettings, true);
    }
}
