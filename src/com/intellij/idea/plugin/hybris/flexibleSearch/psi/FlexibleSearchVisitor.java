/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// This is a generated file. Not intended for manual editing.
package com.intellij.idea.plugin.hybris.flexibleSearch.psi;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import org.jetbrains.annotations.NotNull;

public class FlexibleSearchVisitor extends PsiElementVisitor {

    public void visitAggregateFunction(@NotNull FlexibleSearchAggregateFunction o) {
        visitPsiElement(o);
    }

    public void visitBetweenPredicate(@NotNull FlexibleSearchBetweenPredicate o) {
        visitPsiElement(o);
    }

    public void visitBooleanFactor(@NotNull FlexibleSearchBooleanFactor o) {
        visitPsiElement(o);
    }

    public void visitBooleanPredicand(@NotNull FlexibleSearchBooleanPredicand o) {
        visitPsiElement(o);
    }

    public void visitBooleanPrimary(@NotNull FlexibleSearchBooleanPrimary o) {
        visitPsiElement(o);
    }

    public void visitBooleanTerm(@NotNull FlexibleSearchBooleanTerm o) {
        visitPsiElement(o);
    }

    public void visitBooleanTest(@NotNull FlexibleSearchBooleanTest o) {
        visitPsiElement(o);
    }

    public void visitBooleanValueExpression(@NotNull FlexibleSearchBooleanValueExpression o) {
        visitPsiElement(o);
    }

    public void visitCharacterLikePredicate(@NotNull FlexibleSearchCharacterLikePredicate o) {
        visitPsiElement(o);
    }

    public void visitCharacterPattern(@NotNull FlexibleSearchCharacterPattern o) {
        visitPsiElement(o);
    }

    public void visitCharacterStringLiteral(@NotNull FlexibleSearchCharacterStringLiteral o) {
        visitPsiElement(o);
    }

    public void visitCharacterSubstringFunction(@NotNull FlexibleSearchCharacterSubstringFunction o) {
        visitPsiElement(o);
    }

    public void visitCharacterValueFunction(@NotNull FlexibleSearchCharacterValueFunction o) {
        visitPsiElement(o);
    }

    public void visitColumnReference(@NotNull FlexibleSearchColumnReference o) {
        visitPsiElement(o);
    }

    public void visitCommonValueExpression(@NotNull FlexibleSearchCommonValueExpression o) {
        visitPsiElement(o);
    }

    public void visitCompOp(@NotNull FlexibleSearchCompOp o) {
        visitPsiElement(o);
    }

    public void visitCorrelationName(@NotNull FlexibleSearchCorrelationName o) {
        visitPsiElement(o);
    }

    public void visitDerivedColumn(@NotNull FlexibleSearchDerivedColumn o) {
        visitPsiElement(o);
    }

    public void visitExistsPredicate(@NotNull FlexibleSearchExistsPredicate o) {
        visitPsiElement(o);
    }

    public void visitFromClause(@NotNull FlexibleSearchFromClause o) {
        visitPsiElement(o);
    }

    public void visitGeneralLiteral(@NotNull FlexibleSearchGeneralLiteral o) {
        visitPsiElement(o);
    }

    public void visitGeneralSetFunction(@NotNull FlexibleSearchGeneralSetFunction o) {
        visitPsiElement(o);
    }

    public void visitGroupByClause(@NotNull FlexibleSearchGroupByClause o) {
        visitPsiElement(o);
    }

    public void visitGroupingColumnReference(@NotNull FlexibleSearchGroupingColumnReference o) {
        visitPsiElement(o);
    }

    public void visitGroupingColumnReferenceList(@NotNull FlexibleSearchGroupingColumnReferenceList o) {
        visitPsiElement(o);
    }

    public void visitGroupingElement(@NotNull FlexibleSearchGroupingElement o) {
        visitPsiElement(o);
    }

    public void visitGroupingElementList(@NotNull FlexibleSearchGroupingElementList o) {
        visitPsiElement(o);
    }

    public void visitJoinCondition(@NotNull FlexibleSearchJoinCondition o) {
        visitPsiElement(o);
    }

    public void visitJoinSpecification(@NotNull FlexibleSearchJoinSpecification o) {
        visitPsiElement(o);
    }

    public void visitJoinType(@NotNull FlexibleSearchJoinType o) {
        visitPsiElement(o);
    }

    public void visitJoinedTable(@NotNull FlexibleSearchJoinedTable o) {
        visitPsiElement(o);
    }

    public void visitLang(@NotNull FlexibleSearchLang o) {
        visitPsiElement(o);
    }

    public void visitLikePredicate(@NotNull FlexibleSearchLikePredicate o) {
        visitPsiElement(o);
    }

    public void visitNullOrdering(@NotNull FlexibleSearchNullOrdering o) {
        visitPsiElement(o);
    }

    public void visitNullPredicate(@NotNull FlexibleSearchNullPredicate o) {
        visitPsiElement(o);
    }

    public void visitOrderByClause(@NotNull FlexibleSearchOrderByClause o) {
        visitPsiElement(o);
    }

    public void visitOrderingSpecification(@NotNull FlexibleSearchOrderingSpecification o) {
        visitPsiElement(o);
    }

    public void visitOrdinaryGroupingSet(@NotNull FlexibleSearchOrdinaryGroupingSet o) {
        visitPsiElement(o);
    }

    public void visitPredicate(@NotNull FlexibleSearchPredicate o) {
        visitPsiElement(o);
    }

    public void visitQuerySpecification(@NotNull FlexibleSearchQuerySpecification o) {
        visitPsiElement(o);
    }

    public void visitRowValuePredicand(@NotNull FlexibleSearchRowValuePredicand o) {
        visitPsiElement(o);
    }

    public void visitSearchCondition(@NotNull FlexibleSearchSearchCondition o) {
        visitPsiElement(o);
    }

    public void visitSelectList(@NotNull FlexibleSearchSelectList o) {
        visitPsiElement(o);
    }

    public void visitSelectSublist(@NotNull FlexibleSearchSelectSublist o) {
        visitPsiElement(o);
    }

    public void visitSetFunctionType(@NotNull FlexibleSearchSetFunctionType o) {
        visitPsiElement(o);
    }

    public void visitSetQuantifier(@NotNull FlexibleSearchSetQuantifier o) {
        visitPsiElement(o);
    }

    public void visitSortKey(@NotNull FlexibleSearchSortKey o) {
        visitPsiElement(o);
    }

    public void visitSortSpecification(@NotNull FlexibleSearchSortSpecification o) {
        visitPsiElement(o);
    }

    public void visitSortSpecificationList(@NotNull FlexibleSearchSortSpecificationList o) {
        visitPsiElement(o);
    }

    public void visitStringValueExpression(@NotNull FlexibleSearchStringValueExpression o) {
        visitPsiElement(o);
    }

    public void visitStringValueFunction(@NotNull FlexibleSearchStringValueFunction o) {
        visitPsiElement(o);
    }

    public void visitSubquery(@NotNull FlexibleSearchSubquery o) {
        visitPsiElement(o);
    }

    public void visitTableExpression(@NotNull FlexibleSearchTableExpression o) {
        visitPsiElement(o);
    }

    public void visitTableName(@NotNull FlexibleSearchTableName o) {
        visitPsiElement(o);
    }

    public void visitTablePrimary(@NotNull FlexibleSearchTablePrimary o) {
        visitPsiElement(o);
    }

    public void visitTableReference(@NotNull FlexibleSearchTableReference o) {
        visitPsiElement(o);
    }

    public void visitTableReferenceList(@NotNull FlexibleSearchTableReferenceList o) {
        visitPsiElement(o);
    }

    public void visitTableSubquery(@NotNull FlexibleSearchTableSubquery o) {
        visitPsiElement(o);
    }

    public void visitTruthValue(@NotNull FlexibleSearchTruthValue o) {
        visitPsiElement(o);
    }

    public void visitValueExpression(@NotNull FlexibleSearchValueExpression o) {
        visitPsiElement(o);
    }

    public void visitWhereClause(@NotNull FlexibleSearchWhereClause o) {
        visitPsiElement(o);
    }

    public void visitPsiElement(@NotNull PsiElement o) {
        visitElement(o);
    }

}
