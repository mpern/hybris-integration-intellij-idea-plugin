/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.runConfigurations;

import com.intellij.execution.junit.JUnitConfiguration;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Computable;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.search.EverythingGlobalScope;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author Eugene.Kudelevsky
 */
class FilteringScope extends EverythingGlobalScope {

    @Nullable
    private final ClassAnnotationTestFilter annotationTestFilter;
    @Nullable
    private final ScopeTestFilter scopeTestFilter;
    @Nullable
    private final Module filteringModule;

    @SuppressWarnings("CastToConcreteClass")
    public FilteringScope(@NotNull final JUnitConfiguration configuration) {
        super(configuration.getProject());
        final HybrisTestConfiguration hybrisConfiguration = (HybrisTestConfiguration) configuration;
        annotationTestFilter = hybrisConfiguration.getClassAnnotationFilter();
        scopeTestFilter = hybrisConfiguration.getScopeTestFilter();
        filteringModule = hybrisConfiguration.getFilteringModule();
    }

    @Override
    public boolean contains(@NotNull final VirtualFile file) {
        if (annotationTestFilter == null && scopeTestFilter == null) {
            return true;
        }
        final Project project = getProject();
        assert project != null;

        return ApplicationManager.getApplication().runReadAction((Computable<Boolean>) () ->
            (annotationTestFilter == null || annotationTestFilter.check(project, file)) &&
            (scopeTestFilter == null || scopeTestFilter.check(project, file, filteringModule)));
    }
}
