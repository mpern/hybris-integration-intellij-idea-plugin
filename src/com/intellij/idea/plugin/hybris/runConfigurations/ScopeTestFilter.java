/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.runConfigurations;

import com.intellij.idea.plugin.hybris.project.descriptors.HybrisModuleDescriptor;
import com.intellij.idea.plugin.hybris.project.descriptors.HybrisModuleDescriptorType;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtilCore;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.intellij.idea.plugin.hybris.common.utils.HybrisI18NBundleUtils.message;

/**
 * @author Eugene.Kudelevsky
 */
public enum ScopeTestFilter {
    MODULE(message("hybris.tests.single.module.filter.name")),
    ALL_CUSTOM_MODULES(message("hybris.tests.custom.modules.filter.name"));

    @NotNull
    private final String name;

    ScopeTestFilter(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public boolean check(
        @NotNull final Project project,
        @NotNull final VirtualFile file,
        @Nullable final Module filteringModule
    ) {
        final Module containingModule = ModuleUtilCore.findModuleForFile(file, project);

        if (containingModule == null) {
            return false;
        }
        switch (this) {
            case MODULE:
                return containingModule.equals(filteringModule);
            case ALL_CUSTOM_MODULES:
                return HybrisModuleDescriptor.getDescriptorType(containingModule) == HybrisModuleDescriptorType.CUSTOM;
            default:
                return false;
        }
    }
}
